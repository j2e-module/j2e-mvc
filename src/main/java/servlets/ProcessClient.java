package servlets;

import DAO.ClientDAO;
import models.ClientBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/processClient")
public class ProcessClient extends javax.servlet.http.HttpServlet{

    private ClientDAO clientDao;

    @Override
    public void init() {
        clientDao = new ClientDAO(getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/process.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ClientBean client = (ClientBean) request.getSession().getAttribute("clientBean");
        try {
            int rowsAffected = clientDao.addClient(client);
            if(rowsAffected > 0){
                response.sendRedirect("success");
                return;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        response.sendRedirect("error");
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ClientBean client = (ClientBean) request.getSession().getAttribute("clientBean");
        try {
            int rowsAffected = clientDao.updateClient(client);
            if(rowsAffected > 0){
                response.sendRedirect("success");
                return;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        response.sendRedirect("error");
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long id = Long.parseLong(request.getParameter("id"));
        try {
            int rowsAffected = clientDao.deleteClient(id);
            if(rowsAffected > 0){
                response.sendRedirect("success");
                return;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        response.sendRedirect("error");
    }
}
