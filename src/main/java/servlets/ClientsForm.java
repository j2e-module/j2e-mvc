package servlets;

import DAO.ClientDAO;
import models.ClientBean;
import wrappers.MethodOverrideWrapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/clientsForm")
public class ClientsForm extends HttpServlet {

    private ClientDAO clientDao;

    @Override
    public void init() {
        clientDao = new ClientDAO(getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("clientBean", null);
        if(request.getParameter("id") != null){
            long id = Long.parseLong(request.getParameter("id"));
            try {
                ClientBean client = clientDao.getClient(id);
                request.setAttribute("clientBean", client);
                if (client == null) {
                    response.getWriter().println("No client data found");
                    return;
                }
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        request.getRequestDispatcher("/WEB-INF/views/clientForm.jsp").forward(request, response);
    }
}
