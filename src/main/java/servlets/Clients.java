package servlets;

import DAO.ClientDAO;
import pojos.Client;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/clients")
public class Clients extends HttpServlet {

    private ClientDAO clientDao;

    @Override
    public void init() {
        clientDao = new ClientDAO(getServletContext());
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            List<Client> clients = clientDao.getClients();
            request.setAttribute("clients", clients);
            request.getRequestDispatcher("/WEB-INF/views/clients.jsp").forward(request, response);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
