package wrappers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class MethodOverrideWrapper extends HttpServletRequestWrapper {

    private final String method;

    public MethodOverrideWrapper(HttpServletRequest request, String method) {
        super(request);
        this.method = method;
    }

    @Override
    public String getMethod() {
        return method.toUpperCase();
    }
}
