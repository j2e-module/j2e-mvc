package DAO;

import models.ClientBean;
import pojos.Client;

import javax.servlet.ServletContext;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientDAO {

    private final Connection connection;
    public ClientDAO(ServletContext servletContext) {
        this.connection = (Connection) servletContext.getAttribute("dbConnection");
    }
    public int addClient(ClientBean client) throws ClassNotFoundException {
        String INSERT_CLIENT_SQL = "INSERT INTO clients " +
                "(first_name, last_name, age) " +
                "VALUES (?, ?, ?);";
        int result = 0;
        try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_CLIENT_SQL)) {
            preparedStatement.setString(1, client.getFirstName());
            preparedStatement.setString(2, client.getLastName());
            preparedStatement.setInt(3, client.getAge());
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            result = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int updateClient(ClientBean client) throws ClassNotFoundException {
        String UPDATE_CLIENT_SQL = "UPDATE clients " +
                "SET first_name = ?, last_name = ?, age = ? " +
                "WHERE id = ?;";
        int result = 0;
        try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CLIENT_SQL)) {
            preparedStatement.setString(1, client.getFirstName());
            preparedStatement.setString(2, client.getLastName());
            preparedStatement.setInt(3, client.getAge());
            preparedStatement.setLong(4, client.getId());
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            result = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<Client> getClients() throws ClassNotFoundException {
        String GET_CLIENTS_SQL = "SELECT * FROM clients;";
        List<Client> clients = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_CLIENTS_SQL)) {
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Client client = new Client();
                client.setId(rs.getLong("id"));
                client.setFirstName(rs.getString("first_name"));
                client.setLastName(rs.getString("last_name"));
                client.setAge(rs.getInt("age"));
                clients.add(client);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clients;
    }

    public ClientBean getClient(long id) throws ClassNotFoundException {
        String GET_CLIENT_SQL = "SELECT * FROM clients WHERE id = ?;";
        ClientBean client = new ClientBean();
        try (PreparedStatement preparedStatement = connection.prepareStatement(GET_CLIENT_SQL)) {
            preparedStatement.setLong(1, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();
            if(!rs.isBeforeFirst()) return null;
            while (rs.next()) {
                client.setId(rs.getLong("id"));
                client.setFirstName(rs.getString("first_name"));
                client.setLastName(rs.getString("last_name"));
                client.setAge(rs.getInt("age"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return client;
    }

    public int deleteClient(Long id) throws ClassNotFoundException {
        String DELETE_CLIENT_SQL = "DELETE FROM clients WHERE id = ?;";
        int result = 0;
        try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_CLIENT_SQL)) {
            preparedStatement.setLong(1, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            result = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
