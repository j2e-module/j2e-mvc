package filters;

import models.ClientBean;
import wrappers.MethodOverrideWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/processClient/*")
public class ClientFilter extends HttpFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String method = request.getParameter("_method");
        if(method != null && !method.equalsIgnoreCase("GET")){
            MethodOverrideWrapper requestWrapper = new MethodOverrideWrapper(request, method);
            chain.doFilter(requestWrapper, response);
            return;
        }
        chain.doFilter(request, response);
    }
}
