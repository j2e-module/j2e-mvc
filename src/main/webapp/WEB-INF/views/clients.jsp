<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Clients list</title>
</head>
<body>
    <h1>Clients list</h1>
    <table border="1">
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Age</th>
            <th>Actions</th>
        </tr>
        <c:forEach var="client" items="${clients}">
            <tr>
                <td>${client.firstName}</td>
                <td>${client.lastName}</td>
                <td>${client.age}</td>
                <td>
                <div style="display: flex; flex-direction: row">
                    <form action="/clientsForm" method="get" >
                        <input type="hidden" name="id" value="${client.id}">
                        <input type="submit" value="Edit">
                    </form>
                    <form action="/processClient">
                        <input type="hidden" name="id" value="${client.id}">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="submit" method="get" value="Delete">
                    </form>
                </div>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <a href="/clientsForm">Add client</a>
</body>
</html>
