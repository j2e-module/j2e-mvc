<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="clientBean" class="models.ClientBean" scope="session"/>
<jsp:setProperty name="clientBean" property="*"/>
<%
out.print("This " + clientBean);
%>
<c:choose>
    <c:when test="${clientBean.getId() != null}">
        will be updated!
        <br />
    </c:when>
    <c:otherwise>
        will be added!
        <br />
    </c:otherwise>
</c:choose>
<html>
<head>
    <title>Clients Form</title>
</head>
<body>
    <form action="processClient" method="post">
        <input type="hidden" name="_method" value="<%= clientBean.getId() != null ? "put" : "post" %>" />
        <input type="submit" value="Apply">
    </form>
</body>
</html>