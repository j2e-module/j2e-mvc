<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Clients Form</title>
</head>
<body>
    <form action="processClient" method="get">
        <input type="hidden" name="id" value="${clientBean.id}" />
        <label for="firstName">First Name:</label><br>
        <input id="firstName" type="text" name="firstName" value="${clientBean.firstName}"><br>
        <label for="lastName">Last Name:</label><br>
        <input id="lastName" type="text" name="lastName" value="${clientBean.lastName}"><br>
        <label for="age">Age:</label><br>
        <input id="age" type="number" name="age" value="${clientBean.age}"><br>
        <input type="submit" value="<%= request.getParameter("id") != null ? "Update" : "Add" %>">
    </form>
</body>
</html>
